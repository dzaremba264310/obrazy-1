/* Dawid Zaremba 264310*/
//Sprawozdanie z zadania 3. Data wykonanian 03.12.2021
//Kod zrodłowy skompilowanego programu home/dzaremba/sprawoOdczyt.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 512       /* Maksymalny rozmiar wczytywanego obrazu */
#define DL_LINII 1024 /* Dlugosc buforow pomocniczych */

/************************************************************************************
 * Funkcja wczytuje obraz PGM z pliku do tablicy       	       	       	       	    *
 *										    *
 * \param[in] plik_we uchwyt do pliku z obrazem w formacie PGM			    *
 * \param[out] obraz_pgm tablica, do ktorej zostanie zapisany obraz		    *
 * \param[out] wymx szerokosc obrazka						    *
 * \param[out] wymy wysokosc obrazka						    *
 * \param[out] szarosci liczba odcieni szarosci					    *
 * \return liczba wczytanych pikseli						    *
 ************************************************************************************/

int czytaj(FILE *plik_we, int obraz_pgm[][MAX], int *wymx, int *wymy, int *szarosci)
{
  char buf[DL_LINII]; /* bufor pomocniczy do czytania naglowka i komentarzy */
  int znak;           /* zmienna pomocnicza do czytania komentarzy */
  int koniec = 0;     /* czy napotkano koniec danych w pliku */
  int i, j;

  /*Sprawdzenie czy podano prawidlowy uchwyt pliku */
  if (plik_we == NULL)
  {
    fprintf(stderr, "Blad: Nie podano uchwytu do pliku\n");
    return (0);
  }

  /* Sprawdzenie "numeru magicznego" - powinien byc P2 */
  if (fgets(buf, DL_LINII, plik_we) == NULL) /* Wczytanie pierwszej linii pliku do bufora */
    koniec = 1;                              /* Nie udalo sie? Koniec danych! */

  if ((buf[0] != 'P') || (buf[1] != '2') || koniec)
  { /* Czy jest magiczne "P2"? */
    fprintf(stderr, "Blad: To nie jest plik PGM\n");
    return (0);
  }

  /* Pominiecie komentarzy */
  do
  {
    if ((znak = fgetc(plik_we)) == '#')
    {                                            /* Czy linia rozpoczyna sie od znaku '#'? */
      if (fgets(buf, DL_LINII, plik_we) == NULL) /* Przeczytaj ja do bufora                */
        koniec = 1;                              /* Zapamietaj ewentualny koniec danych */
    }
    else
    {
      ungetc(znak, plik_we);        /* Gdy przeczytany znak z poczatku linii */
    }                               /* nie jest '#' zwroc go                 */
  } while (znak == '#' && !koniec); /* Powtarzaj dopoki sa linie komentarza */
                                    /* i nie nastapil koniec danych         */

  /* Pobranie wymiarow obrazu i liczby odcieni szarosci */
  if (fscanf(plik_we, "%d %d %d", wymx, wymy, szarosci) != 3)
  {
    fprintf(stderr, "Blad: Brak wymiarow obrazu lub liczby stopni szarosci\n");
    return (0);
  }
  /* Pobranie obrazu i zapisanie w tablicy obraz_pgm*/
  for (i = 0; i < *wymy; i++)
  {
    for (j = 0; j < *wymx; j++)
    {
      if (fscanf(plik_we, "%d", &(obraz_pgm[i][j])) != 1)
      {
        fprintf(stderr, "Blad: Niewlasciwe wymiary obrazu\n");
        return (0);
      }
    }
  }
  return *wymx * *wymy; /* Czytanie zakonczone sukcesem    */
} /* Zwroc liczbe wczytanych pikseli */

/* Wyswietlenie obrazu o zadanej nazwie za pomoca programu "display"   */
void wyswietl(char *n_pliku)
{
  char polecenie[DL_LINII]; /* bufor pomocniczy do zestawienia polecenia */

  strcpy(polecenie, "display "); /* konstrukcja polecenia postaci */
  strcat(polecenie, n_pliku);    /* display "nazwa_pliku" &       */
  strcat(polecenie, " &");
  printf("%s\n", polecenie); /* wydruk kontrolny polecenia */
  system(polecenie);         /* wykonanie polecenia        */
}

void zapis(FILE *plik_wy, int obraz_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i, j;
  fprintf(plik_wy,"P2\n");
  fprintf(plik_wy," %d %d\n", wymx,wymy);
  fprintf(plik_wy," %d\n", szarosci);

  for(i=0; i<wymy; i++)
  {
    for(j=0; j<wymx; j++)
    {
      fprintf(plik_wy," %d", obraz_pgm[i][j]);
    }
    fprintf(plik_wy,"\n ");
  }
}

void negatyw(int obraz_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i, j;

  for(i=0; i<wymy; i++)
  {
    for(j=0; j<wymx; j++)
    {
      obraz_pgm[i][j]=szarosci-obraz_pgm[i][j];
    }
    
  }
}

void progowanie(int obraz_pgm[][MAX],int wymx, int wymy, int szarosci)
{
  int i,j;

  for(i=0; i<wymy; i++)
  {
    for(j=0; j<wymx; j++)
    {
      if (obraz_pgm[i][j]<=0.5*szarosci)
      {obraz_pgm[i][j]=0;}
      else
      {obraz_pgm[i][j]=szarosci;}
    }
  }
}

void konturowanie(int obraz_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i,j;

  for (i=0; i<wymy; i++)
  {
    for(j=0; j<wymx; j++)
    {
      obraz_pgm[i][j]=abs(obraz_pgm[i+1][j]-obraz_pgm[i][j])+abs(obraz_pgm[i][j+1]-obraz_pgm[i][j]);
    }
  }
}

void rozmycie_poziome(int obraz_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i,j;
  for(i=1; i<wymy-1; i++)
  {
    for(j=0; j<wymx; j++)
    {
      obraz_pgm[i][j]=(float)1/3*(obraz_pgm[i-1][j]+obraz_pgm[i][j]+obraz_pgm[i+1][j]);
    }
  }
}

void rozmycie_pionowe(int obraz_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i, j;
  for(i=0; i<wymy; i++)
  {
    for(j=1; j<wymx-1; j++)
    {
      obraz_pgm[i][j]=(float)1/3*(obraz_pgm[i][j-1]+obraz_pgm[i][j]+obraz_pgm[i][j+1]);
    }
  }
}

void rozciagniecie_histogramu(int obraz_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i,j,min,max;
  min=255;
  max=0;
  for(i=0; i<wymy; i++)
  {
    for(j=0; j<wymx; j++)
    {
      if(obraz_pgm[i][j]<min)
      {
        min=obraz_pgm[i][j];
      }
      if(obraz_pgm[i][j]>max)
      {
        max=obraz_pgm[i][j];
      }
    }
  }
  for(i=0; i<wymy; i++)
  {
    for(j=1; j<wymx-1; j++)
    {
      obraz_pgm[i][j]=(obraz_pgm[i][j]-min)*(szarosci/(max-min));
    }
  }
}

void zmiana_poziomow(int obram_pgm[][MAX], int wymx, int wymy, int szarosci)
{
  int i,j,czern,biel;
  czern=0.2*szarosci, biel=0.8*szarosci;
  for(i=0; i<wymy; i++)
  {
    for(j=1; j<wymx-1; j++)
    {
      if(obram_pgm[i][j]<=czern)
      {obram_pgm[i][j]=0;}
      
      if (obram_pgm[i][j]>czern && obram_pgm[i][j]<biel) 
      {obram_pgm[i][j]=(obram_pgm[i][j]-czern)*(szarosci/(biel-czern));} 
      
      if(obram_pgm[i][j]>=biel)
      {obram_pgm[i][j]=szarosci;}
    }
  }


}

int main()
{
  int obraz[MAX][MAX];
  int wymx, wymy, odcieni, wybor;

  int odczytano = 0;
  FILE *plik;
  char nazwa[100];

  
/* Wczytanie zawartosci wskazanego pliku do pamieci */
        printf("Podaj nazwe pliku:\n");
        scanf("%s", nazwa);
        plik = fopen(nazwa, "r");

        if (plik != NULL)
               { /* co spowoduje zakomentowanie tego warunku */
                  odczytano = czytaj(plik, obraz, &wymx, &wymy, &odcieni);
                  fclose(plik);
               }

 do 
    {
      if (plik == NULL)
               {
                   printf("Podany plik nie istnieje. Spróbuj ponownie.\n");
                   return 0;
               }
        printf("\nMenu:\n\t 1 - Negatyw \n\t 2 - Progowanie \n\t 3 - Konturowanie \n\t 4 - Rozmycie poziome \n\t 5 - Rozmycie pionowe");
        printf("\n\t 6 - Rozciagniecie histogramu \n\t 7 - Zmiana poziomow \n\t 8 - Wyjdź");
        printf("\nTwój wybór:");
        scanf(" %d", &wybor);
        
        switch(wybor)
        {
            case(1):
                printf("Wybrano negatyw\n");
                negatyw(obraz,wymx,wymy,odcieni);
                break;
            case(2):
                printf("Wybrano progowanie\n");
                progowanie(obraz,wymx,wymy,odcieni);
                break;
            case(3):
                printf("Wybrano konturowanie\n");
                konturowanie(obraz, wymx, wymy, odcieni);
                break;
            case(4):
                printf("Wybrano rozmycie poziome\n");
                rozmycie_poziome(obraz,wymx,wymy,odcieni); 
                break;  
            case(5):
                printf("Wybrano rozmycie pionowe\n");
                rozmycie_pionowe(obraz,wymx,wymy,odcieni); 
                break;  
            case(6):
                printf("Wybrano rozciagniecie histogramu\n");
                rozciagniecie_histogramu(obraz,wymx,wymy,odcieni); 
                break; 
            case(7):
                printf("Wybrano zmiana poziomow\n");
                zmiana_poziomow(obraz,wymx,wymy,odcieni); 
                break; 
            case(8):
                printf("Koniec działania programu.\n");
                exit(1);
                default: printf("\nNie ma opcji %d do wyboru. Spróbuj ponownie.", wybor); 
                break;
        }
    }
    while((wybor<1)||(wybor>7));


/* Wczytanie zawartosci do wskazanego pliku*/
  printf("Podaj nazwe pliku:\n");
  scanf("%s", nazwa);
  plik = fopen(nazwa, "w");

    zapis(plik, obraz, wymx, wymy, odcieni);
    fclose(plik);


  /* Wyswietlenie poprawnie wczytanego obrazu zewnetrznym programem */
  if (odczytano != 0)
    wyswietl(nazwa);

  return odczytano;
}


/*Poniższe testy maja za zadanie sprawdzic poprawnosci dzialania powyzszego programu, umozliwiajacego edycje obrazow restrowych,
ktory to przedstawia je w postaci tablicy dwuwymiarowej, w ktorej kazda liczba reprezentuje jeden piksel, kolor czarny
reprezentuje liczba '0', a kolor bialy wartosc maksymalna, zalezna od liczby poziomow szarosci. Program wczytuje plik, po tym
jak uzytkownik wprowadzi do terminala jego nazwe, zapisuje zmieniony juz obraz pod nazwa wskazana przez uztkownika, 
a nastepnie otwiera go za pomoca programu display.
W programie zostalo rowniez zaimplementowane menu, aby uzytkownik mogl bezproblemowo wybierac funkcje edycji obrazu.
Wyniki testow oznaczonych '*', sa porownywane z oczekiwanymi dzialaniami konkretnych funkcji, przedstawionych w pliku
https://kcir.pwr.edu.pl/~mucha/PProg/PProg_lab_03/obrazy_filtry.pdf, testy te sa przeprowadzane na tym samym obrazie
kubus.pgm*/

/*   Test 1.)
  - Sprawdzenie, czy funkcja wczytaj i zapisz dziala poprawnie oraz czy obraz zostanie wyswietlony przez program display
  Dane wejsciowe: kubus.pgm, kub1.pgm
  Obraz został poprawnie wyswietlony oraz zapisany pod wskazana nazwa w katalogu domowym, w przypadku wprowadzenia blednej 
  nazwy pliku lub pliku w zlym formacie, uzytkownik zostaje poinformowanny o bledzie, a program konczy dzialanie.

    Test 2.)
  - Sprawdzenie poprawnosci dzialania menu
  Dane wejsciowe: kubus.pgm, kub1.pgm
  Po wprowadzeniu kazdej z dostapnych opcji wywolana zostaje inna funkcja, obraz zostaje prawidlowo zapisany i wyswietlony,
  wyjatek stanowi wybor opcji '8', program konczy wtedy dzialanie bez wyswietlenie i zapisu obrazu. Problematyczne 
  sa niestety przypadki, gdy uzytkownik omylkowo wprowadzi np. litere zamiast cyfry, program wpada wtedy w nieskonczona petle.

    Test 3.)*
  - Sprawdzenie poprawnosci dzialania funkcji "negatyw"
  Dane wejsciowe: kubus.pgm, kubneg.pgm
  Negatyw zostaje poprawnie wykonany, wynik koncowy zgadza sie z tym zamieszczonym w pliku.

  Test 4.)*
  - Sprawdzenie poprawnosci dzialania funkcji "progowanie"
  Dane wejsciowe: kubus.pgm, kubneg.pgm
  Progrowanie zostaje poprawnie wykonane, wynik koncowy zgadza sie z tym zamieszczonym w pliku. Prog zostal 
  ustawiony na 50%.

  Test 5.)*
  - Sprawdzenie poprawnosci dzialania funkcji "konturowanie"
  Dane wejsciowe: kubus.pgm, kubneg.pgm
  Konturowanie zostaje poprawnie wykonane, wyswietla sie wyrazny kontur obrazka wejsciowego, a wynik 
  koncowy zgadza sie z tym zamieszczonym w pliku.

  Test 6.)
  - Sprawdzenie poprawnosci dzialania funkcji "rozciagniecie histogramu"
  Dane wejsciowe: CitroenC5.pgm, cithist.pgm
  Test ten zostal wyjatkowo przeprowadzony na innym pliku poniewaz kubus.pgm zawiera w sobie pelen zakres szarosci wiec 
  efekt dzialania funkcji nie bylby widoczny. Skala szarosci pliku wejsciowego zawierala sie w przedziale <3, 255>, plik wyjsciowy
  posiadal skale w zakresie <0, 255>, a wiec funkcja dziala poprawnie

  Test 7.)*
  - Sprawdzenie poprawnosci dzialania funkcji "zmiana poziomow"
  Dane wejsciowe: kubus.pgm, kubneg.pgm
  Zmiana poziomow zostaje poprawnie wykonana, wynik koncowy zgadza sie z tym zamieszczonym w pliku. 
  Biel i Czern zostaly odpowiednio ustawione na 20% i 80% z maskymalnej wartosci poziomu szarosci.

  Test 8.)*
  - Sprawdzenie poprawnosci dzialania funkcji "rozmycie pioziome"
  Dane wejsciowe: kubus.pgm, kubneg.pgm
  Rozmycie poziome zostaje poprawnie wykonane, wynik koncowy zgadza sie z tym zamieszczonym w pliku. Promien rozmycia 
  wynosi 1.

  Test 9.)*
  - Sprawdzenie poprawnosci dzialania funkcji "rozmycie pionowe"
  Dane wejsciowe: kubus.pgm, kubneg.pgm
  Rozmycie pionowe zostaje poprawnie wykonane, wynik koncowy zgadza sie z tym zamieszczonym w pliku. Promien rozmycia 
  wynosi 1.

  Podsumowanie: Przeprowadzone testy nie wykazaly zadnych bledow, poza przypadkiem gdy uzytkownik w menu omylkowo 
        wprowadzi np. litere zamiast cyfry, program wpada wtedy w nieskonczona petle i uniemozliwia 
        korzystanie z niego, dlatego nalezaloby wyeliminowac ten blad.
*/
